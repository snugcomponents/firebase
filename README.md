## Installation

To install the latest version of `snugcomponents/firebase` use [Composer](https://getcomposer.org).

```bash
composer require snugcomponents/firebase
```

## Documentation

For details on how to use this package, check out our [documentation](.docs).

## Versions

| State  | Version  | Branch | Nette | PHP     |
|--------|----------|--------|-------|---------|
| stable | `^1.0.0` | `main` | 4.0+  | `>=8.2` |

## Development

This package is currently maintaining by these authors.

<a href="https://gitlab.com/miellap">
  <img alt="Miellap" width="80" height="80" src="https://gitlab.com/uploads/-/system/user/avatar/14030435/avatar.png?width=400">
</a>

-----

## Conclusion
This package requires PHP 8.2, Nette 4.0, and it is property of SnugDesign © 2024
