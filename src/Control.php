<?php declare(strict_types=1);

namespace Snugcomponents\Firebase;

use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Nette\Application\UI\Form;
use Nette\Forms\Controls\BaseControl;
use Snugcomponents\Form\FormControl;
use Snugcomponents\Form\FormFactory;
use Kreait\Firebase\Exception\Messaging\NotFound;
use Tracy\Debugger;

class Control extends FormControl
{
    public function __construct(
        FormFactory                   $formFactory,
        private readonly Factory      $firebaseFactory,
        private readonly DataProvider $dataProvider,
        private readonly string       $firebaseTokenNotFoundErrorMessage,
    ) {
        parent::__construct($formFactory);
        $this->setTemplateScriptsFile(__DIR__ . '/templates/scripts.latte');
    }

    public function render(...$args): void
    {
        $args['firebaseConfigUrl'] = $this->dataProvider->getFirebaseConfigUrl();
        $args['vapidKey'] = $this->dataProvider->getVapidKey();

        parent::render(...$args);
    }

    public function createComponentForm(): Form
    {
        $form = parent::createComponentForm();
        $form->addHidden('firebaseToken')
            ->setHtmlAttribute('class', 'firebase-token')
            ->addRule($this->isTokenValid(...), $this->firebaseTokenNotFoundErrorMessage);
        return $form;
    }

    private function isTokenValid(BaseControl $hidden): bool
    {
        $token = $hidden->getValue();
        $kreaitFactory = $this->firebaseFactory;
        $kreaitFactory->createAuth();
        $messaging = $kreaitFactory->createMessaging();

        $message = CloudMessage::fromArray([
            'token' => $token,
        ]);

        try {
            $messaging->send($message, true);
        } catch (NotFound $e) {
            Debugger::log($e);
            return false;
        }

        return true;
    }
}
