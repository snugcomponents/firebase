<?php declare(strict_types=1);

namespace Snugcomponents\Firebase;

interface ControlFactory
{
    public function create(): Control;
}
