<?php declare(strict_types=1);

namespace Snugcomponents\Firebase;

interface DataProvider
{
    public function getVapidKey(): string;

    public function getFirebaseConfigUrl(): string;

    public function getDocumentRootDirPath(): string;

    public function getPrivateKeyJsonPath(): string;

}

