<?php declare(strict_types=1);

namespace Snugcomponents\Firebase;

use Kreait\Firebase\Factory;
use Nette;
use Nette\DI\CompilerExtension;
use Nette\DI\Definitions\Definition;
use Nette\DI\MissingServiceException;
use Nette\Schema\Expect;
use Nette\Schema\Schema;
use Nette\Utils\Strings;
use Snugcomponents\Cache\CacheItemPoolFactory;
use Snugcomponents\Form\FormFactory;

class FirebaseExtension extends CompilerExtension
{
    public function getConfigSchema(): Schema
    {
        $baseStructure = [
            'formFactory' => Expect::string()
                ->required(),
            'cacheNamespace' => Expect::string('Snugcomponents\Firebase'),
            'firebaseTokenNotFoundErrorMessage' => Expect::string('firebase.token.notFoundErrorMessage'),
            'cacheItemPoolFactory' => Expect::string(CacheItemPoolFactory::class),
        ];

        return Expect::anyOf(
            Expect::structure([
                ...$baseStructure,
                'autowired' => Expect::bool(true),
            ]),
            Expect::arrayOf(
                Expect::structure([
                    ...$baseStructure,
                    'autowired' => Expect::bool(false),
                ]),
                Expect::string(),
            )->transform($this->arraySetAutowired(...)),
        );
    }

    private function arraySetAutowired(array $array): array
    {
        foreach ($array as $value) {
            if ($value->autowired === true) {
                return $array;
            }
        }

        $key = array_key_first($array);
        $array[$key]->autowired = true;

        return $array;
    }

    private function getConfigAsArray(): array
    {
        $config = $this->config;

        if (!is_array($config)) {
            $config = ['main' => $config];
        }

        $retVal = [];

        foreach ($config as $name => $values) {
            $retVal[$this->prefix($name)] = $values;
        }

        return $retVal;
    }

    public function loadConfiguration(): void
    {
        $builder = $this->getContainerBuilder();

        $config = $this->getConfigAsArray();

        foreach ($config as $name => $values) {
            $builder->addDefinition($name . '.firebase.factory')
                ->setFactory(Factory::class);

            $builder->addFactoryDefinition($name . '.formControl.factory')
                ->setImplement(ControlFactory::class);
        }
    }


    /**
     * @throws \Exception
     */
    public function beforeCompile(): void
    {
        $builder = $this->getContainerBuilder();

        $config = $this->getConfigAsArray();
        $dataProviderDefinition = $builder->getDefinitionByType(DataProvider::class);

        foreach ($config as $name => $values) {
            $cacheItemPoolFactoryDefinition = $this->getDefinitionByConfig(
                configName: $values->cacheItemPoolFactory,
                parentClass: CacheItemPoolFactory::class,
            );
            $formFactoryDefinition = $this->getDefinitionByConfig(
                configName: $values->formFactory,
                parentClass: FormFactory::class,
            );

            $builder->getDefinition($name . '.firebase.factory')
                ->addSetup(
                    entity: '? = ?->withServiceAccount(?->getPrivateKeyJsonPath())',
                    args: [
                        '@self',
                        '@self',
                        $dataProviderDefinition,
                    ],
                )
                ->addSetup(
                    entity: '? = ?->withAuthTokenCache(?->create(?))',
                    args: [
                        '@self',
                        '@self',
                        $cacheItemPoolFactoryDefinition,
                        $values->cacheNamespace,
                    ],
                );

            $builder->getDefinition($name . '.formControl.factory')
                ->getResultDefinition()
                ->setFactory(
                    factory: Control::class,
                    args: [
                        'formFactory' => $formFactoryDefinition,
                        'firebaseTokenNotFoundErrorMessage' => $values->firebaseTokenNotFoundErrorMessage,
                    ]);
        }

        parent::beforeCompile();
    }

    private function getDefinitionByConfig(string $configName, string $parentClass): Definition
    {
        $originConfigName = $configName;
        $builder = $this->getContainerBuilder();

        if (str_starts_with($configName, '@')) {
            $name = Strings::after($configName, '@');
            $definition = $builder->getDefinition($name);
            $configName = $definition->getType();
        } else {
            $definition = $builder->getDefinitionByType($configName);
        }

        if (!is_a(
            object_or_class: $configName,
            class: $parentClass,
            allow_string: true,
        )) {
            throw new MissingServiceException(
                sprintf(
                    'Service \'%s\' with config name \'%s\' needs to be an instance of \'%s\'',
                    $configName,
                    $originConfigName,
                    $parentClass,
                ),
            );
        }

        return $definition;
    }
}
