
importScripts("https://www.gstatic.com/firebasejs/10.8.0/firebase-app-compat.js");
importScripts("https://www.gstatic.com/firebasejs/10.8.0/firebase-messaging-compat.js");
importScripts('Here you need to put your firebaseConfig URI address');

firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();

messaging.onBackgroundMessage((payload) => {
  console.log('onBackgroundMessage ', payload);

  // Send message to all open clients (tabs)
  self.clients.matchAll().then(clients => {
    console.log('Sending message to all clients');
    console.log(clients)
    clients.forEach(client => {
      console.log('Sending message to client');
      client.postMessage({
        msg: "NotificationReceived",
        payload: payload
      });
    });
  });
});
