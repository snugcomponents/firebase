
const app = firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();

var snugcomponents = snugcomponents || {};
snugcomponents.firebase = snugcomponents.firebase || {};

// Properties
snugcomponents.firebase.$formToSend = null;
snugcomponents.firebase.vapidKey = snugcomponents.firebase.vapidKey || '';

// Methods
snugcomponents.firebase.init = function() {
    this._addNavigatorEventListener();

    if (!("Notification" in window)) {
        console.log('This browser does not support desktop notification');
        return;
    }

    this._addPermissionRequestButtonClickEventListener();

    if (Notification.permission === "granted") {
        console.log('Notification permission already granted.');
        return;
    }

    if (Notification.permission === "denied") {
        console.log('Notification permission are denied.');
        return;
    }


    console.log('Notification permission not granted.', 'Asking for permission...');
    $('.request-permission-button').first().click();
};

snugcomponents.firebase.printNotification = function(payload) {
    console.log('onBackgroundMessage ', payload);
};

snugcomponents.firebase._printNotification = function(payload) {
    snugcomponents.firebase.printNotification(payload);
};

snugcomponents.firebase._addNavigatorEventListener = function() {
    if (!("navigator" in window)) {
        console.log('This browser does not support navigator and service worker.');
        return;
    }

    messaging.onMessage(snugcomponents.firebase._printNotification);

    navigator.serviceWorker.addEventListener('message', event => {
        console.log('Received a message from service worker: ', event.data);

        if (event.data.msg && event.data.msg === "NotificationReceived") {
            // Handle notification received, you can access payload using event.data.payload
            console.log('Notification received in the background', event.data.payload);

            // Perform actions to update UI or handle the notification data
            snugcomponents.firebase.printNotification(event.data.payload);
        }
    });
};

snugcomponents.firebase._addPermissionRequestButtonClickEventListener = function() {
    $(document).on('click', '.request-permission-button', function(e) {
        e.preventDefault();

        snugcomponents.firebase.$formToSend = $(this).parents('form');

        console.log('Requesting permission...');

        Notification.requestPermission()
            .then(snugcomponents.firebase._requestPermissionsHandler);
    });
};

snugcomponents.firebase._requestPermissionsHandler = function(permission) {
    if (permission !== 'granted') {
        console.log('Unable to get permission to notify.');
        return;
    }

    console.log('Notification permission granted.');

    navigator.serviceWorker.register('/firebase-messaging-sw.js', {
        scope: '/'
    }).then(
        snugcomponents.firebase._registrationWorkerHandler
    ).catch(function(err) {
        console.error('Service worker registration failed: ', err);
    });
};

snugcomponents.firebase._registrationWorkerHandler = function(registration) {
    console.log('Service worker registration successful with scope: ', registration.scope);

    messaging
        .getToken({ vapidKey: snugcomponents.firebase.vapidKey, serviceWorkerRegistration: registration })
        .then(
            snugcomponents.firebase._messagingGetTokenHandler
        ).catch((err) => {
        console.log(err);
    });
};

snugcomponents.firebase._messagingGetTokenHandler = function(token) {
    if (!token) {
        console.log('No Instance ID token available. Request permission to generate one.');
        return;
    }

    console.log(token);
    $form = snugcomponents.firebase.$formToSend;

    $form
        .find('.firebase-token')
        .val(token);

    $form.submit();
};

// Init
snugcomponents.firebase.init();
